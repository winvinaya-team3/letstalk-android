package com.example.letstalk_android.api_client;

import com.example.letstalk_android.models.TextResponse;
import com.example.letstalk_android.models.VideoReferenceModel;

import java.util.List;

import io.reactivex.Observable;
import okhttp3.MultipartBody;
import retrofit2.http.GET;
import retrofit2.http.Multipart;
import retrofit2.http.POST;
import retrofit2.http.Part;
import retrofit2.http.Query;

public interface LetsTalkAPIService {

    String BASE_URL = "http://192.168.1.6:5000/letstalk/";

    @GET("getISL/")
    Observable<List<VideoReferenceModel>> getISLVideos(@Query("sentence") String sentence);

    @Multipart
    @POST("getText/")
    Observable<TextResponse> getTranslatedText(@Part MultipartBody.Part video, @Query("markers") String markers);

}

package com.example.letstalk_android.fragments;

import android.Manifest;
import android.app.ProgressDialog;
import android.content.res.Configuration;
import android.media.MediaActionSound;
import android.os.Bundle;
import android.speech.tts.TextToSpeech;
import android.util.Log;
import android.util.Size;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.MimeTypeMap;
import android.widget.Button;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.ActionBarDrawerToggle;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.camera.core.Camera;
import androidx.camera.core.CameraSelector;
import androidx.camera.core.Preview;
import androidx.camera.core.VideoCapture;
import androidx.camera.core.impl.VideoCaptureConfig;
import androidx.camera.lifecycle.ProcessCameraProvider;
import androidx.camera.view.PreviewView;
import androidx.core.content.ContextCompat;
import androidx.drawerlayout.widget.DrawerLayout;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.LifecycleOwner;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProvider;
import androidx.navigation.NavController;
import androidx.navigation.NavOptions;
import androidx.navigation.fragment.NavHostFragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.example.letstalk_android.R;
import com.example.letstalk_android.adapters.ISLTextAdapter;
import com.example.letstalk_android.api_client.LetsTalkAPIService;
import com.example.letstalk_android.models.TextResponse;
import com.example.letstalk_android.viewmodel.ISLToTextLogViewModel;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.android.material.navigation.NavigationView;
import com.google.common.util.concurrent.ListenableFuture;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.iceteck.silicompressorr.SiliCompressor;

import java.io.File;
import java.net.URISyntaxException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Date;
import java.util.Locale;
import java.util.Objects;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.Executor;
import java.util.concurrent.Executors;

import io.reactivex.Observable;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.schedulers.Schedulers;
import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;

import static com.example.letstalk_android.MainActivity.hasPermissions;
import static com.example.letstalk_android.MainActivity.retrofit;

public class ISLToTextFragment extends Fragment {

    private DrawerLayout mDrawer;
    private NavigationView nvDrawer;
    private Toolbar toolbar;
    private ActionBarDrawerToggle drawerToggle;
    private AppCompatActivity activity;
    private NavController navController;

    private RecyclerView recyclerView;
    private RecyclerView.Adapter recyclerViewAdapter;
    private RecyclerView.LayoutManager layoutManager;
    private ArrayList<TextResponse> responseLogList;

    private ProgressDialog progressDialog;

    private PreviewView viewFinder;
    private FloatingActionButton recordSendButton;
    private Button addMarkerButton;
    private View recordBlinkView;
    private Executor executor;
    private Preview preview;
    private CameraSelector cameraSelector;
    private VideoCapture videoCapture;
    private Boolean recording = false;
    private Boolean cameraReady = false;
    private ArrayList<Long> cutTimestamps;
    private MediaActionSound cameraSound;

    private TextToSpeech textToSpeech;
    private boolean ttsInitialized;

    private FirebaseAuth mAuth;
    private FirebaseUser currentUser;
    private FirebaseDatabase database;

    private LetsTalkAPIService letsTalkAPIService;

    private String PERMISSIONS[] = {
            Manifest.permission.CAMERA,
            Manifest.permission.READ_EXTERNAL_STORAGE,
            Manifest.permission.WRITE_EXTERNAL_STORAGE,
            Manifest.permission.RECORD_AUDIO
    };

    public ISLToTextFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        if (!hasPermissions(getContext(), PERMISSIONS)) {
            requestPermissions(PERMISSIONS, 101);
        }

        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_i_s_l_to_text, container, false);

        mAuth = FirebaseAuth.getInstance();
        currentUser = mAuth.getCurrentUser();
        database = FirebaseDatabase.getInstance();

        cameraReady = false;
        ttsInitialized = false;
        cameraSound = new MediaActionSound();

        letsTalkAPIService = retrofit.create(LetsTalkAPIService.class);

        progressDialog = new ProgressDialog(getContext());
        progressDialog.setCanceledOnTouchOutside(false);

        navController = NavHostFragment.findNavController(this);

        viewFinder = view.findViewById(R.id.view_finder);

        recordSendButton = view.findViewById(R.id.record_send_button);
        recordSendButton.setImageDrawable(getResources().getDrawable(R.drawable.ic_baseline_fiber_manual_record_24_red));

        addMarkerButton = view.findViewById(R.id.add_marker_button);
        recordBlinkView = view.findViewById(R.id.blink_view);
        recordBlinkView.setVisibility(View.INVISIBLE);

        recyclerView = view.findViewById(R.id.word_recycler_view);

        layoutManager = new LinearLayoutManager(getContext());
        recyclerView.setLayoutManager(layoutManager);

        responseLogList = new ArrayList<>();

        recyclerViewAdapter = new ISLTextAdapter(responseLogList, this);
        recyclerView.setAdapter(recyclerViewAdapter);

        ISLToTextLogViewModel logViewModel = new ViewModelProvider(this).get(ISLToTextLogViewModel.class);
        LiveData<DataSnapshot> liveData = logViewModel.getDataSnapshotLiveData();
        liveData.observe(getViewLifecycleOwner(), new Observer<DataSnapshot>() {
            @Override
            public void onChanged(DataSnapshot dataSnapshot) {
                responseLogList.clear();
                long count = dataSnapshot.getChildrenCount();
                for (DataSnapshot snapshot : dataSnapshot.getChildren()) {
                    TextResponse model = snapshot.getValue(TextResponse.class);
                    responseLogList.add(model);
                }
                Collections.sort(responseLogList);
                recyclerViewAdapter.notifyDataSetChanged();
                recyclerView.scrollToPosition(responseLogList.size() - 1);
            }
        });

        recordSendButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (cameraReady && !recording && hasPermissions(getContext(), PERMISSIONS)) {
                    recording = true;
                    addMarkerButton.setEnabled(true);
                    recordBlinkView.setVisibility(View.VISIBLE);
                    recordSendButton.setImageDrawable(getResources().getDrawable(R.drawable.ic_baseline_fiber_manual_record_24_green));
                    cutTimestamps = new ArrayList<>();
                    cutTimestamps.add(System.currentTimeMillis());
                    File outputDir = getContext().getCacheDir();
                    File outputFile;
                    try {
                        outputFile = File.createTempFile("" + System.currentTimeMillis(), "_" + currentUser.getUid() + ".mp4", outputDir);
                    } catch (Exception e) {
                        outputFile = null;
                    }
                    cameraSound.play(MediaActionSound.START_VIDEO_RECORDING);
                    videoCapture.startRecording(outputFile, executor, new VideoCapture.OnVideoSavedCallback() {
                        @Override
                        public void onVideoSaved(@NonNull File file) {
                            Log.d("Video saved: ", file.getAbsolutePath());
                            cameraSound.play(MediaActionSound.STOP_VIDEO_RECORDING);
//                            String filePath = null; // use this file to send compressed video file
//                            try {
////                                filePath = SiliCompressor.with(getContext()).compressVideo(file.getAbsolutePath(), outputDir.getAbsolutePath());
////                                Log.i("TAGTAG", filePath);
//                            } catch (URISyntaxException e) {
//                                e.printStackTrace();
//                            }
                            makeApiCall(file);
                        }

                        @Override
                        public void onError(int videoCaptureError, @NonNull String message, @Nullable Throwable cause) {
                            Log.d("LetsTalk: ", "Unable to Save Video");
                        }
                    });
                } else if (cameraReady && hasPermissions(getContext(), PERMISSIONS)) {
                    recording = false;
                    addMarkerButton.setEnabled(false);
                    recordBlinkView.setVisibility(View.INVISIBLE);
                    recordSendButton.setImageDrawable(getResources().getDrawable(R.drawable.ic_baseline_fiber_manual_record_24_red));
                    videoCapture.stopRecording();
                } else {
                    Toast.makeText(getContext(), "Grant Camera, Audio and Storage Permissions!", Toast.LENGTH_SHORT).show();
                }
            }
        });

        addMarkerButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                cutTimestamps.add(System.currentTimeMillis());
                Toast.makeText(getContext(), "Marker Added.", Toast.LENGTH_SHORT).show();
            }
        });

        textToSpeech = new TextToSpeech(getContext(), new TextToSpeech.OnInitListener() {
            @Override
            public void onInit(int i) {
                if (i != TextToSpeech.ERROR) {
                    ttsInitialized = true;
                    textToSpeech.setLanguage(Locale.ENGLISH);
                }
            }
        });

        mDrawer = view.findViewById(R.id.drawer_layout);
        nvDrawer = view.findViewById(R.id.navView);

        toolbar = view.findViewById(R.id.toolbar);
        if (getActivity() != null) {
            activity = (AppCompatActivity) getActivity();
            activity.setSupportActionBar(toolbar);
            activity.getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        }

        drawerToggle = setupDrawerToggle();
        drawerToggle.setDrawerIndicatorEnabled(true);
        drawerToggle.syncState();
        mDrawer.addDrawerListener(drawerToggle);

        setupDrawerContent(nvDrawer);

        executor = Executors.newSingleThreadExecutor();

        startCamera();

        return view;
    }

    public void speakLogAloud(TextResponse textResponse) {
        if (ttsInitialized) {
            textToSpeech.speak(textResponse.getText(), TextToSpeech.QUEUE_FLUSH, null);
        }
    }

    private void startCamera() {

        final ListenableFuture<ProcessCameraProvider> cameraProviderFuture = ProcessCameraProvider.getInstance(getContext());

        cameraProviderFuture.addListener(new Runnable() {
            @Override
            public void run() {
                try {
                    ProcessCameraProvider cameraProvider = cameraProviderFuture.get();
                    bindPreview(cameraProvider);
                } catch (ExecutionException | InterruptedException e) {
                }
            }
        }, ContextCompat.getMainExecutor(getContext()));

    }

    private void makeApiCall(File file) {
        StringBuilder cutTimestampsStrBuilder = new StringBuilder();
        String cutTimestampsStr = "";
        if (cutTimestamps.size() > 1) {
            Long zeroTime = cutTimestamps.get(0);
            for (int i = 1; i < cutTimestamps.size(); i++) {
                cutTimestampsStrBuilder.append((cutTimestamps.get(i) - zeroTime));
                cutTimestampsStrBuilder.append(",");
            }
            cutTimestampsStr = cutTimestampsStrBuilder.substring(0, cutTimestampsStrBuilder.length() - 1);
        }

        // create request body
        RequestBody requestBody = RequestBody.create(
                MediaType.parse(
                        Objects.requireNonNull(
                                MimeTypeMap
                                        .getSingleton()
                                        .getMimeTypeFromExtension(
                                                MimeTypeMap
                                                        .getFileExtensionFromUrl(file
                                                                .toURI()
                                                                .toString())))),
                file);

        // create form-data
        MultipartBody.Part multipartBodyPart = MultipartBody.Part.createFormData("file", file.getName(), requestBody);

        Observable<TextResponse> textResponseObservable = letsTalkAPIService.getTranslatedText(multipartBodyPart, cutTimestampsStr);
        if (!progressDialog.isShowing()) {
            progressDialog.setMessage("Translating. Please Wait...");
            getActivity().runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    progressDialog.show();
                }
            });
        }

        textResponseObservable.subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(this::handleResults, this::handleErrors);
    }

    private void handleResults(TextResponse textResponse) {
        if (progressDialog.isShowing()) {

            SimpleDateFormat format = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");
            Date date = Calendar.getInstance().getTime();
            String formattedDate = format.format(date);
            textResponse.setDateString(formattedDate);

            DatabaseReference reference = database.getReference("profile_data").child(currentUser.getUid()).child("isl_to_text");
            reference.push().setValue(textResponse).addOnFailureListener(new OnFailureListener() {
                @Override
                public void onFailure(@NonNull Exception e) {
                    Toast.makeText(getContext(), "Unable to log data on firebase.", Toast.LENGTH_SHORT).show();
                }
            });

            getActivity().runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    progressDialog.dismiss();
                }
            });
        }
        if (textResponse != null) {
            Log.i("LetsTalk: ", "ISLToText Response: " + textResponse.getText());
        }
    }

    private void handleErrors(Throwable t) {
        if (progressDialog.isShowing()) {
            getActivity().runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    progressDialog.dismiss();
                }
            });
        }
        Toast.makeText(getContext(), "Error Fetching API Response. Try again.", Toast.LENGTH_SHORT).show();
    }

    private void bindPreview(@NonNull ProcessCameraProvider cameraProvider) {
        preview = new Preview.Builder().build();
        cameraSelector = new CameraSelector.Builder().requireLensFacing(CameraSelector.LENS_FACING_BACK).build();
        videoCapture = new VideoCaptureConfig.Builder().setMaxResolution(new Size(480, 480)).setCameraSelector(cameraSelector).build();
        preview.setSurfaceProvider(viewFinder.createSurfaceProvider());
        Camera camera = cameraProvider.bindToLifecycle((LifecycleOwner) this, cameraSelector, preview, videoCapture);
        cameraReady = true;
    }

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {

        if (drawerToggle.onOptionsItemSelected(item)) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onConfigurationChanged(@NonNull Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
        drawerToggle.onConfigurationChanged(newConfig);
    }

    public void selectDrawerItem(MenuItem menuItem) {

        switch (menuItem.getItemId()) {
            case R.id.nav_english_to_isl:
                navController.navigate(R.id.action_ISLToTextFragment_to_textToISLFragment,
                        null,
                        new NavOptions.Builder().setPopUpTo(R.id.ISLToTextFragment, true).build());
                break;
            case R.id.nav_isl_to_english:
                break;
        }

        // Close the navigation drawer
        mDrawer.closeDrawers();
    }

    private void setupDrawerContent(NavigationView navigationView) {
        navigationView.setNavigationItemSelectedListener(
                new NavigationView.OnNavigationItemSelectedListener() {
                    @Override
                    public boolean onNavigationItemSelected(MenuItem menuItem) {
                        selectDrawerItem(menuItem);
                        return true;
                    }
                });
    }

    private ActionBarDrawerToggle setupDrawerToggle() {
        // NOTE: Make sure you pass in a valid toolbar reference.  ActionBarDrawToggle() does not require it
        // and will not render the hamburger icon without it.
        return new ActionBarDrawerToggle(activity, mDrawer, toolbar, R.string.drawer_open, R.string.drawer_close);
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
    }

    @Override
    public void onPause() {
        if (textToSpeech != null) {
            textToSpeech.stop();
            textToSpeech.shutdown();
        }
        super.onPause();
    }
}

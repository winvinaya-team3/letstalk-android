package com.example.letstalk_android.fragments;


import android.Manifest;
import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.content.res.Configuration;
import android.graphics.drawable.Drawable;
import android.media.MediaPlayer;
import android.net.Uri;
import android.os.Bundle;
import android.speech.RecognizerIntent;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.RelativeLayout;
import android.widget.Toast;
import android.widget.VideoView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.ActionBarDrawerToggle;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.core.view.GravityCompat;
import androidx.drawerlayout.widget.DrawerLayout;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProvider;
import androidx.navigation.NavController;
import androidx.navigation.NavOptions;
import androidx.navigation.fragment.NavHostFragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.amplifyframework.core.Amplify;
import com.amplifyframework.core.Consumer;
import com.amplifyframework.storage.result.StorageDownloadFileResult;
import com.example.letstalk_android.R;
import com.example.letstalk_android.adapters.LogAdapter;
import com.example.letstalk_android.api_client.LetsTalkAPIService;
import com.example.letstalk_android.models.LogModel;
import com.example.letstalk_android.models.VideoReferenceModel;
import com.example.letstalk_android.viewmodel.TextToISLLogViewModel;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.android.material.navigation.NavigationView;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

import java.io.File;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Date;
import java.util.List;
import java.util.Locale;

import io.reactivex.Observable;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.schedulers.Schedulers;

import static com.example.letstalk_android.MainActivity.hasPermissions;
import static com.example.letstalk_android.MainActivity.retrofit;

public class TextToISLFragment extends Fragment {

    private static final int REQUEST_CODE_SPEECH_INPUT = 1000;
    private DrawerLayout mDrawer;
    private NavigationView nvDrawer;
    private Toolbar toolbar;
    private ActionBarDrawerToggle drawerToggle;
    private AppCompatActivity activity;
    private NavController navController;

    private VideoView videoView;

    private EditText textBox;
    private FloatingActionButton recordSendButton;
    private RecyclerView logRecyclerView;
    private Drawable recordDrawable, sendDrawable;
    private ProgressDialog progressDialog;

    private LogAdapter logAdapter;
    private ArrayList<LogModel> logModelArrayList;
    private LinearLayoutManager layoutManager;

    private FirebaseDatabase database;
    private FirebaseUser currentUser;

    private LetsTalkAPIService letsTalkAPIService;
    private ArrayList<Uri> urlList;

    private Boolean sendBool = false;
    private Boolean videoResized = false;

    private String[] PERMISSIONS = {
            Manifest.permission.READ_EXTERNAL_STORAGE,
            Manifest.permission.WRITE_EXTERNAL_STORAGE,
            Manifest.permission.RECORD_AUDIO
    };

    public TextToISLFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        if (!hasPermissions(getContext(), PERMISSIONS)) {
            requestPermissions(PERMISSIONS, 102);
        }

        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_text_to_i_s_l, container, false);

        letsTalkAPIService = retrofit.create(LetsTalkAPIService.class);

        progressDialog = new ProgressDialog(getContext());
        progressDialog.setCanceledOnTouchOutside(false);

        videoResized = false;

        navController = NavHostFragment.findNavController(this);

        database = FirebaseDatabase.getInstance();
        currentUser = FirebaseAuth.getInstance().getCurrentUser();

        recordDrawable = getResources().getDrawable(R.drawable.ic_baseline_mic_24);
        sendDrawable = getResources().getDrawable(R.drawable.ic_sign_language);

        videoView = view.findViewById(R.id.video_view);
        videoView.setVisibility(View.GONE);

        textBox = view.findViewById(R.id.text_box);
        recordSendButton = view.findViewById(R.id.record_send_button);

        logRecyclerView = view.findViewById(R.id.text_to_isl_log);
        logModelArrayList = new ArrayList<>();
        logAdapter = new LogAdapter(logModelArrayList, this);
        layoutManager = new LinearLayoutManager(getContext());
        logRecyclerView.setLayoutManager(layoutManager);
        logRecyclerView.setAdapter(logAdapter);

        textBox.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                if (charSequence.length() != 0 && !sendBool) {
                    sendBool = true;
                    recordSendButton.setImageDrawable(sendDrawable);
                } else if (charSequence.length() == 0 && sendBool) {
                    sendBool = false;
                    recordSendButton.setImageDrawable(recordDrawable);
                }
            }

            @Override
            public void afterTextChanged(Editable editable) {

            }
        });

        mDrawer = view.findViewById(R.id.drawer_layout);
        nvDrawer = view.findViewById(R.id.navView);

        toolbar = view.findViewById(R.id.toolbar);
        if (getActivity() != null) {
            activity = (AppCompatActivity) getActivity();
            activity.setSupportActionBar(toolbar);
            activity.getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        }

        drawerToggle = setupDrawerToggle();
        drawerToggle.setDrawerIndicatorEnabled(true);
        drawerToggle.syncState();
        mDrawer.addDrawerListener(drawerToggle);

        setupDrawerContent(nvDrawer);

        TextToISLLogViewModel logViewModel = new ViewModelProvider(this).get(TextToISLLogViewModel.class);
        LiveData<DataSnapshot> liveData = logViewModel.getDataSnapshotLiveData();
        liveData.observe(getViewLifecycleOwner(), new Observer<DataSnapshot>() {
            @Override
            public void onChanged(DataSnapshot dataSnapshot) {
                logModelArrayList.clear();
                for (DataSnapshot snapshot : dataSnapshot.getChildren()) {
                    LogModel model = new LogModel(snapshot.getValue().toString(), snapshot.getKey());
                    logModelArrayList.add(model);
                }
                Collections.sort(logModelArrayList);
                logAdapter.notifyDataSetChanged();
                logRecyclerView.scrollToPosition(logModelArrayList.size() - 1);
            }
        });

        recordSendButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if (sendBool && hasPermissions(
                        getContext(),
                        Manifest.permission.READ_EXTERNAL_STORAGE,
                        Manifest.permission.WRITE_EXTERNAL_STORAGE)) {

                    String sentenceString = textBox.getText().toString().trim();
                    textBox.setText("");
                    if (!sentenceString.isEmpty()) {
                        SimpleDateFormat format = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");
                        Date date = Calendar.getInstance().getTime();
                        String formattedDate = format.format(date);
                        LogModel logModel = new LogModel(formattedDate, sentenceString);

                        DatabaseReference reference = database.getReference("profile_data").child(currentUser.getUid()).child("text_to_isl");
                        reference.child(sentenceString).setValue(formattedDate);
                        makeApiCall(sentenceString);
                    }

                } else if (sendBool) {
                    Toast.makeText(getContext(), "Grant Storage Permission!", Toast.LENGTH_SHORT).show();
                } else {
                    if (hasPermissions(getContext(), Manifest.permission.RECORD_AUDIO)) {
                        speak();
                    } else {
                        Toast.makeText(getContext(), "Grant Audio Recording Permission!", Toast.LENGTH_SHORT).show();
                    }
                }
            }
        });

        return view;
    }

    public void translateString(String sentenceString) {
        if (!sentenceString.isEmpty() && hasPermissions(
                getContext(),
                Manifest.permission.READ_EXTERNAL_STORAGE,
                Manifest.permission.WRITE_EXTERNAL_STORAGE)) {

            makeApiCall(sentenceString);

        }
    }

    private void playVideos() {

        if (!videoResized) {
            RelativeLayout.LayoutParams layoutParams = new RelativeLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
            layoutParams.addRule(RelativeLayout.CENTER_HORIZONTAL);
            layoutParams.addRule(RelativeLayout.ALIGN_PARENT_TOP);
            layoutParams.setMargins(18, 18, 18, 18);
            videoView.setLayoutParams(layoutParams);

            layoutParams = new RelativeLayout.LayoutParams(logRecyclerView.getWidth(), logRecyclerView.getHeight());
            layoutParams.setMargins(18, 18, 18, 18);
            layoutParams.addRule(RelativeLayout.BELOW, R.id.video_view);
            layoutParams.addRule(RelativeLayout.ABOVE, R.id.text_interface_layout);
            logRecyclerView.setLayoutParams(layoutParams);

            videoView.setVisibility(View.VISIBLE);
            logRecyclerView.invalidate();
        }

        if (urlList.size() > 0) {

            videoView.setVideoURI(urlList.get(0));
            videoView.requestFocus();
            videoView.setOnCompletionListener(new MediaPlayer.OnCompletionListener() {
                @Override
                public void onCompletion(MediaPlayer mediaPlayer) {
                    if (urlList.size() > 0) {
                        videoView.setVideoURI(urlList.get(0));
                        videoView.requestFocus();
                        videoView.start();
                        urlList.remove(0);
                    }
                }
            });
            videoView.start();
            urlList.remove(0);
        }
    }

    private void getUrls(List<VideoReferenceModel> referenceModelList) {

        if (!progressDialog.isShowing()) {
            progressDialog.show();
        }

        try {

            File newFile = new File("data/data/" + getActivity().getPackageName() + "/" + referenceModelList.get(0).getReference());
            if (newFile.exists()) {
                urlList.add(Uri.fromFile(newFile));
                referenceModelList.remove(0);
                if (referenceModelList.size() > 0) {
                    getUrls(referenceModelList);
                } else if (urlList.size() > 0) {
                    if (progressDialog.isShowing()) {
                        progressDialog.dismiss();
                    }
                    playVideos();
                } else {
                    if (progressDialog.isShowing()) {
                        progressDialog.dismiss();
                    }
                }
            } else {

                Amplify.Storage.downloadFile(referenceModelList.get(0).getReference(), newFile, new Consumer<StorageDownloadFileResult>() {
                    @Override
                    public void accept(@NonNull StorageDownloadFileResult value) {
                        urlList.add(Uri.fromFile(value.getFile()));
                        referenceModelList.remove(0);
                        if (referenceModelList.size() > 0) {
                            getUrls(referenceModelList);
                        } else if (urlList.size() > 0) {
                            if (progressDialog.isShowing()) {
                                progressDialog.dismiss();
                            }
                            playVideos();
                        } else {
                            if (progressDialog.isShowing()) {
                                progressDialog.dismiss();
                            }
                        }
                    }
                }, error -> Log.e("LetsTalk: ", "Download Failure", error));

            }

        } catch (Exception e) {
            Toast.makeText(getContext(), "Error creating File", Toast.LENGTH_SHORT).show();
        }
    }

    private void makeApiCall(String sentence) {
        Observable<List<VideoReferenceModel>> videoReferenceListObservable = letsTalkAPIService
                .getISLVideos(sentence);
        if (!progressDialog.isShowing()) {
            progressDialog.setMessage("Translating. Please Wait...");
            progressDialog.show();
        }
        videoReferenceListObservable.subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(this::handleResults, this::handleError);
    }

    private void handleResults(List<VideoReferenceModel> referenceModelList) {
        urlList = new ArrayList<>();
        if (referenceModelList != null) {
            getUrls(referenceModelList);
        }
    }

    private void handleError(Throwable t) {
        if (progressDialog.isShowing()) {
            progressDialog.dismiss();
        }
        Toast.makeText(getContext(), "Erron Fetching API Response. Try again.", Toast.LENGTH_SHORT).show();
    }

    // intent to show speech to text dialog
    private void speak() {
        Intent intent = new Intent(RecognizerIntent.ACTION_RECOGNIZE_SPEECH);
        intent.putExtra(RecognizerIntent.EXTRA_LANGUAGE_MODEL, RecognizerIntent.LANGUAGE_MODEL_FREE_FORM);
        intent.putExtra(RecognizerIntent.EXTRA_LANGUAGE, Locale.getDefault());
        intent.putExtra(RecognizerIntent.EXTRA_PROMPT, "Hi Speak Something");
        try {
            startActivityForResult(intent, REQUEST_CODE_SPEECH_INPUT);
        } catch (Exception e) {
            Toast.makeText(getActivity(), "" + e.getMessage(), Toast.LENGTH_SHORT).show();
        }
    }

    public void selectDrawerItem(MenuItem menuItem) {

        switch (menuItem.getItemId()) {
            case R.id.nav_english_to_isl:
                break;
            case R.id.nav_isl_to_english:
                navController.navigate(R.id.action_textToISLFragment_to_ISLToTextFragment,
                        null,
                        new NavOptions.Builder().setPopUpTo(R.id.textToISLFragment, true).build());
                break;
        }

        // Close the navigation drawer
        mDrawer.closeDrawers();
    }

    private void setupDrawerContent(NavigationView navigationView) {
        navigationView.setNavigationItemSelectedListener(
                new NavigationView.OnNavigationItemSelectedListener() {
                    @Override
                    public boolean onNavigationItemSelected(MenuItem menuItem) {
                        selectDrawerItem(menuItem);
                        return true;
                    }
                });
    }

    private ActionBarDrawerToggle setupDrawerToggle() {
        // NOTE: Make sure you pass in a valid toolbar reference.  ActionBarDrawToggle() does not require it
        // and will not render the hamburger icon without it.
        return new ActionBarDrawerToggle(activity, mDrawer, toolbar, R.string.drawer_open, R.string.drawer_close);
    }

    // Handle result from Voice to Text API
    @Override
    public void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        switch (requestCode) {
            case REQUEST_CODE_SPEECH_INPUT: {
                if (resultCode == Activity.RESULT_OK && null != data) {
                    //getting text from intent
                    ArrayList<String> result = data.getStringArrayListExtra(RecognizerIntent.EXTRA_RESULTS);
                    //flushing it to text box
                    String resultStr = (result.get(0) != null) ? result.get(0) : "";
                    textBox.setText(resultStr.trim());
                }
            }
        }

    }

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {

        switch (item.getItemId()) {
            case android.R.id.home:
                mDrawer.openDrawer(GravityCompat.START);
                return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onConfigurationChanged(@NonNull Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
        drawerToggle.onConfigurationChanged(newConfig);
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
    }
}

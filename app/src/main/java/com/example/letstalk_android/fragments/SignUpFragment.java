package com.example.letstalk_android.fragments;

import android.app.ProgressDialog;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.navigation.NavController;
import androidx.navigation.NavOptions;
import androidx.navigation.fragment.NavHostFragment;

import com.example.letstalk_android.R;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;

import static com.example.letstalk_android.MainActivity.DEAF_PREFERENCE;
import static com.example.letstalk_android.MainActivity.sharedPreferences;

public class SignUpFragment extends Fragment {

    private Button signupButton, loginButton;
    private EditText emailEditText, passwordEditText;
    private NavController navController;
    private FirebaseAuth mAuth;
    private Boolean validEmail = false;
    private Boolean validPass = false;

    public SignUpFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_sign_up, container, false);

        navController = NavHostFragment.findNavController(this);

        signupButton = view.findViewById(R.id.signup_button);
        loginButton = view.findViewById(R.id.login_button);

        signupButton.setEnabled(false);
        loginButton.setEnabled(false);

        emailEditText = view.findViewById(R.id.email_edit_text);
        passwordEditText = view.findViewById(R.id.password_edit_text);

        mAuth = FirebaseAuth.getInstance();

        final String emailPattern = "[a-zA-Z0-9._-]+@[a-z]+\\.+[a-z]+";
        emailEditText.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                String email = charSequence.toString();
                validEmail = !email.isEmpty() && email.matches(emailPattern);
                if (validEmail && validPass) {
                    loginButton.setEnabled(true);
                    signupButton.setEnabled(true);
                } else {
                    loginButton.setEnabled(false);
                    signupButton.setEnabled(false);
                }
            }

            @Override
            public void afterTextChanged(Editable editable) {

            }
        });

        passwordEditText.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                String pass = charSequence.toString();
                validPass = pass.length() >= 6;
                if (validEmail && validPass) {
                    loginButton.setEnabled(true);
                    signupButton.setEnabled(true);
                } else {
                    loginButton.setEnabled(false);
                    signupButton.setEnabled(false);
                }
            }

            @Override
            public void afterTextChanged(Editable editable) {

            }
        });

        loginButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {


                final ProgressDialog progressDialog = new ProgressDialog(getContext());
                progressDialog.setCanceledOnTouchOutside(false);
                progressDialog.show();

                String emailStr = emailEditText.getText().toString();
                String passwordStr = passwordEditText.getText().toString();

                final Boolean deafPref = sharedPreferences.getBoolean(DEAF_PREFERENCE, true);
                final Boolean prefExist = sharedPreferences.contains(DEAF_PREFERENCE);

                if (!emailStr.isEmpty() && !passwordStr.isEmpty()) {
                    mAuth.signInWithEmailAndPassword(emailStr, passwordStr).addOnCompleteListener(new OnCompleteListener<AuthResult>() {
                        @Override
                        public void onComplete(@NonNull Task<AuthResult> task) {
                            progressDialog.dismiss();
                            emailEditText.setText("");
                            emailEditText.clearFocus();
                            passwordEditText.setText("");
                            passwordEditText.clearFocus();
                            if (task.isSuccessful()) {
                                if (!prefExist) {
                                    navController.navigate(R.id.action_signUpFragment_to_userTypeFragment,
                                            null,
                                            new NavOptions.Builder().setPopUpTo(R.id.signUpFragment, true).build());
                                } else if (deafPref) {
                                    navController.navigate(R.id.action_signUpFragment_to_textToISLFragment,
                                            null,
                                            new NavOptions.Builder().setPopUpTo(R.id.signUpFragment, true).build());
                                } else {
                                    navController.navigate(R.id.action_signUpFragment_to_ISLToTextFragment,
                                            null,
                                            new NavOptions.Builder().setPopUpTo(R.id.signUpFragment, true).build());
                                }
                            } else {
                                Toast.makeText(getActivity(), "Failed to login.", Toast.LENGTH_SHORT).show();
                            }
                        }
                    });
                } else {
                    Toast.makeText(getActivity(), "Enter both Email and password!", Toast.LENGTH_SHORT).show();
                }
            }
        });

        signupButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                final ProgressDialog progressDialog = new ProgressDialog(getContext());
                progressDialog.setCanceledOnTouchOutside(false);
                progressDialog.show();

                String emailStr = emailEditText.getText().toString();
                String passwordStr = passwordEditText.getText().toString();

                final Boolean deafPref = sharedPreferences.getBoolean(DEAF_PREFERENCE, true);
                final Boolean prefExist = sharedPreferences.contains(DEAF_PREFERENCE);

                if (!emailStr.isEmpty() && !passwordStr.isEmpty()) {
                    mAuth.createUserWithEmailAndPassword(emailStr, passwordStr).addOnCompleteListener(new OnCompleteListener<AuthResult>() {
                        @Override
                        public void onComplete(@NonNull Task<AuthResult> task) {
                            progressDialog.dismiss();
                            emailEditText.setText("");
                            emailEditText.clearFocus();
                            passwordEditText.setText("");
                            passwordEditText.clearFocus();
                            if (task.isSuccessful()) {
                                if (!prefExist) {
                                    navController.navigate(R.id.action_signUpFragment_to_userTypeFragment,
                                            null,
                                            new NavOptions.Builder().setPopUpTo(R.id.signUpFragment, true).build());
                                } else if (deafPref) {
                                    navController.navigate(R.id.action_signUpFragment_to_textToISLFragment,
                                            null,
                                            new NavOptions.Builder().setPopUpTo(R.id.signUpFragment, true).build());
                                } else {
                                    navController.navigate(R.id.action_signUpFragment_to_ISLToTextFragment,
                                            null,
                                            new NavOptions.Builder().setPopUpTo(R.id.signUpFragment, true).build());
                                }
                            } else {
                                Toast.makeText(getActivity(), "Authentication failed.", Toast.LENGTH_SHORT).show();
                            }
                        }
                    });
                } else {
                    Toast.makeText(getActivity(), "Enter both Email and password!", Toast.LENGTH_SHORT).show();
                }
            }
        });

        return view;
    }
}
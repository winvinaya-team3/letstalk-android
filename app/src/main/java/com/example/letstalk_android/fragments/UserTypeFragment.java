package com.example.letstalk_android.fragments;

import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

import androidx.fragment.app.Fragment;
import androidx.navigation.NavController;
import androidx.navigation.NavOptions;
import androidx.navigation.fragment.NavHostFragment;

import com.example.letstalk_android.R;

import static com.example.letstalk_android.MainActivity.DEAF_PREFERENCE;
import static com.example.letstalk_android.MainActivity.sharedPreferences;


public class UserTypeFragment extends Fragment {

    Button yesButton, noButton;
    private NavController navController;

    public UserTypeFragment() {
        // Required empty public constructor
    }


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_user_type, container, false);

        navController = NavHostFragment.findNavController(this);

        yesButton = view.findViewById(R.id.yes_button);
        noButton = view.findViewById(R.id.no_button);

        yesButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                SharedPreferences.Editor editor = sharedPreferences.edit();
                editor.putBoolean(DEAF_PREFERENCE, true);
                editor.apply();

                navController.navigate(R.id.action_userTypeFragment_to_textToISLFragment,
                        null,
                        new NavOptions.Builder().setPopUpTo(R.id.userTypeFragment, true).build());
            }
        });

        noButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                SharedPreferences.Editor editor = sharedPreferences.edit();
                editor.putBoolean(DEAF_PREFERENCE, false);
                editor.apply();

                navController.navigate(R.id.action_userTypeFragment_to_ISLToTextFragment,
                        null,
                        new NavOptions.Builder().setPopUpTo(R.id.userTypeFragment, true).build());
            }
        });

        return view;
    }
}
package com.example.letstalk_android.fragments;

import android.Manifest;
import android.os.Bundle;
import android.os.Handler;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.navigation.NavController;
import androidx.navigation.NavOptions;
import androidx.navigation.fragment.NavHostFragment;

import com.example.letstalk_android.R;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;

import static com.example.letstalk_android.MainActivity.DEAF_PREFERENCE;
import static com.example.letstalk_android.MainActivity.hasPermissions;
import static com.example.letstalk_android.MainActivity.sharedPreferences;


public class SplashScreenFragment extends Fragment {

    private NavController navController;
    private FirebaseAuth mAuth;

    private String PERMISSIONS[] = {
            Manifest.permission.CAMERA,
            Manifest.permission.READ_EXTERNAL_STORAGE,
            Manifest.permission.WRITE_EXTERNAL_STORAGE,
            Manifest.permission.RECORD_AUDIO
    };

    public SplashScreenFragment() {
        // Required empty public constructor
    }


    @Override
    public void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        navController = NavHostFragment.findNavController(this);
        return inflater.inflate(R.layout.fragment_splash_screen, container, false);
    }


    @Override
    public void onResume() {
        super.onResume();
        if (!hasPermissions(getContext(), PERMISSIONS)) {
            requestPermissions(PERMISSIONS, 100);
        } else {
            navigateDelayed();
        }
    }

    private void navigateDelayed() {
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {

                mAuth = FirebaseAuth.getInstance();
                FirebaseUser currentUser = mAuth.getCurrentUser();
                boolean deafPref = sharedPreferences.getBoolean(DEAF_PREFERENCE, true);
                boolean prefExist = sharedPreferences.contains(DEAF_PREFERENCE);

                if (currentUser == null) {
                    navController.navigate(R.id.action_splashScreenFragment_to_signUpFragment,
                            null,
                            new NavOptions.Builder().setPopUpTo(R.id.splashScreenFragment, true).build());
                } else if (!prefExist) {
                    navController.navigate(R.id.action_splashScreenFragment_to_userTypeFragment,
                            null,
                            new NavOptions.Builder().setPopUpTo(R.id.splashScreenFragment, true).build());
                } else if (deafPref) {
                    navController.navigate(R.id.action_splashScreenFragment_to_textToISLFragment,
                            null,
                            new NavOptions.Builder().setPopUpTo(R.id.splashScreenFragment, true).build());
                } else {
                    navController.navigate(R.id.action_splashScreenFragment_to_ISLToTextFragment,
                            null,
                            new NavOptions.Builder().setPopUpTo(R.id.splashScreenFragment, true).build());
                }
            }
        }, 1000);
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
    }
}

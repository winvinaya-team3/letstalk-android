package com.example.letstalk_android.viewmodel;

import androidx.annotation.NonNull;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.ViewModel;

import com.example.letstalk_android.livedata.FirebaseQueryLiveData;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

public class ISLToTextLogViewModel extends ViewModel {

    private static FirebaseAuth auth = FirebaseAuth.getInstance();
    private static FirebaseUser user = auth.getCurrentUser();
    private static final DatabaseReference databaseReference = FirebaseDatabase
            .getInstance()
            .getReference()
            .child("profile_data")
            .child(user.getUid())
            .child("isl_to_text");

    private FirebaseQueryLiveData liveData = new FirebaseQueryLiveData(databaseReference);

    @NonNull
    public LiveData<DataSnapshot> getDataSnapshotLiveData() {
        return liveData;
    }
}

package com.example.letstalk_android.adapters;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.RecyclerView;

import com.example.letstalk_android.R;
import com.example.letstalk_android.fragments.TextToISLFragment;
import com.example.letstalk_android.models.LogModel;

import java.util.ArrayList;

public class LogAdapter extends RecyclerView.Adapter<LogAdapter.MyViewHolder> {

    private ArrayList<LogModel> mDataset;
    private TextToISLFragment textToISLFragment;

    public static class MyViewHolder extends RecyclerView.ViewHolder {
        public CardView cardView;

        public MyViewHolder(CardView v) {
            super(v);
            cardView = v;
        }
    }

    public LogAdapter(ArrayList<LogModel> mDataset, TextToISLFragment textToISLFragment) {
        this.mDataset = mDataset;
        this.textToISLFragment = textToISLFragment;
    }

    @Override
    public LogAdapter.MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        CardView v = (CardView) LayoutInflater.from(parent.getContext())
                .inflate(R.layout.text_to_isl_card, parent, false);

        LogAdapter.MyViewHolder vh = new LogAdapter.MyViewHolder(v);
        return vh;

    }

    @Override
    public void onBindViewHolder(@NonNull LogAdapter.MyViewHolder holder, int position) {
        TextView dateTextView = holder.cardView.findViewById(R.id.date_text_view);
        TextView sentenceTextView = holder.cardView.findViewById(R.id.log_text_view);
        ImageView playIcon = holder.cardView.findViewById(R.id.log_play_icon);
        dateTextView.setText(mDataset.get(position).getDateString());
        sentenceTextView.setText(mDataset.get(position).getSentence());
        playIcon.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                textToISLFragment.translateString(mDataset.get(position).getSentence());
            }
        });
    }

    @Override
    public int getItemCount() {
        return mDataset.size();
    }

}

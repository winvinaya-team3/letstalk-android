package com.example.letstalk_android.adapters;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.RecyclerView;

import com.example.letstalk_android.R;
import com.example.letstalk_android.fragments.ISLToTextFragment;
import com.example.letstalk_android.models.TextResponse;

import java.util.ArrayList;

public class ISLTextAdapter extends RecyclerView.Adapter<ISLTextAdapter.MyViewHolder> {

    private ArrayList<TextResponse> mDataset;

    private ISLToTextFragment islToTextFragment;

    public static class MyViewHolder extends RecyclerView.ViewHolder {
        public CardView cardView;

        public MyViewHolder(CardView v) {
            super(v);
            cardView = v;
        }
    }

    public ISLTextAdapter(ArrayList<TextResponse> mDataset, ISLToTextFragment islToTextFragment) {
        this.mDataset = mDataset;
        this.islToTextFragment = islToTextFragment;
    }

    @Override
    public ISLTextAdapter.MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        CardView v = (CardView) LayoutInflater.from(parent.getContext())
                .inflate(R.layout.isl_text_log_card, parent, false);

        MyViewHolder vh = new MyViewHolder(v);
        return vh;

    }

    @Override
    public void onBindViewHolder(@NonNull MyViewHolder holder, int position) {
        TextResponse textResponse = mDataset.get(position);
        TextView dateTextView = holder.cardView.findViewById(R.id.date_text_view);
        TextView sentenceTextView = holder.cardView.findViewById(R.id.log_text_view);
        ImageView soundIcon = holder.cardView.findViewById(R.id.sound_icon);
        dateTextView.setText(textResponse.getDateString());
        sentenceTextView.setText(textResponse.getText());
        soundIcon.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                islToTextFragment.speakLogAloud(textResponse);
            }
        });
    }

    @Override
    public int getItemCount() {
        return mDataset.size();
    }
}

package com.example.letstalk_android.models;

import com.google.gson.annotations.SerializedName;

public class VideoReferenceModel {

    @SerializedName("word")
    private String word = "";

    @SerializedName("reference")
    private String reference = "";

    VideoReferenceModel() {
    }

    VideoReferenceModel(String word, String reference) {
        this.word = word;
        this.reference = reference;
    }

    public String getReference() {
        return reference;
    }

    public String getWord() {
        return word;
    }
}

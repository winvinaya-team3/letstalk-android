package com.example.letstalk_android.models;

import com.google.gson.annotations.SerializedName;

public class TextResponse implements Comparable<TextResponse> {

    @SerializedName("text")
    private String text = "";

    private String dateString = "";

    TextResponse() {
    }

    TextResponse(String text) {
        this.text = text;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public String getDateString() {
        return dateString;
    }

    public void setDateString(String dateString) {
        this.dateString = dateString;
    }

    @Override
    public int compareTo(TextResponse textResponse) {
        return getDateString().compareTo(textResponse.getDateString());
    }
}

package com.example.letstalk_android.models;

public class LogModel implements Comparable<LogModel> {

    private String dateString = "";
    private String sentence = "";

    public LogModel() {
    }

    public LogModel(String dateString, String sentence) {
        this.dateString = dateString;
        this.sentence = sentence;
    }

    public String getDateString() {
        return dateString;
    }

    public String getSentence() {
        return sentence;
    }

    @Override
    public int compareTo(LogModel logModel) {
        return getDateString().compareTo(logModel.getDateString());
    }
}
